import 'package:flutter/material.dart';

class SlideDots extends StatelessWidget {
  bool isActive;
  SlideDots(this.isActive);
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(microseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 10),
      height: isActive ? 12 : 5,
      width: isActive ? 12 : 5,
      decoration: BoxDecoration(
          color: isActive ? Colors.purple : Colors.black,
          borderRadius: BorderRadius.all(Radius.circular(12))),
    );
  }
}
