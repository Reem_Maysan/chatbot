import 'package:chatbot/models/slide.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SlideItem extends StatelessWidget {
  final int index;

  const SlideItem({Key key, this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: ScreenUtil().setWidth(800),
            height: ScreenUtil().setHeight(800),
            child: SvgPicture.asset(slideList[index].imageUrl),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(50),
          ),
          Text(
            slideList[index].title,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(50), color: Colors.purple,fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(50),
          ),
          Text(
            slideList[index].description,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(30), color: Colors.purple),
          ),
        ],
      ),
    );
  }
}
