import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Slide {
  final String imageUrl;
  final String title;
  final String description;

  Slide(
      {@required this.imageUrl,
      @required this.title,
      @required this.description});
}

final slideList = [
  Slide(
      imageUrl: "assets/2_q.svg",
      description:
          "Facing a hard time finding what you're looking for\nOn the internet as quickly as possible?",
      title: "Hi there!"),
  Slide(
      imageUrl: "assets/2_c.svg",
      description:
          "Which can be entertaining, helpful, available all the\nTime with a character of your own making.",
      title: "How about a bot friend?"),
  Slide(
      imageUrl: "assets/3_cc.svg",
      description: "'Talk To Me' is a smart chatbot which can make your life easier!\nWanna know more? click Get Started",
      title: "Meet TTM"),
];
