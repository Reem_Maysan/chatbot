import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF6F35A5);
const kPrimaryLightColor = Color(0xFFF1E6FF);


final TextStyle selectedTabStyle = TextStyle(
  fontSize: 20.0,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);
final TextStyle defaultTabStyle = TextStyle(
  fontSize: 20.0,
  color: Colors.white70,
  fontWeight: FontWeight.bold,
);