import 'dart:async';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
//import 'package:audioplayers/audio_cache.dart';
//import 'package:audioplayers/audioplayers.dart';
import 'package:chatbot/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';
import 'package:progress_indicators/progress_indicators.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AudioPlayer audioPlayer;
  AudioCache musicCache;
  bool firstEnter = true;

  @override
  void initState() {
    super.initState();
    audioPlayer = AudioPlayer();
    if (firstEnter) {
      play();
      firstEnter = false;
    }
    //  audioPlayer = AudioPlayer();
    //   audioCache = AudioCache(fixedPlayer: audioPlayer);
    //  audioPlayer.play("url");
/* 
    AssetsAudioPlayer.newPlayer().open(
      Audio("assets/go.m4a"),
      autoStart: true,
      loopMode: LoopMode.playlist
      // autoPlay: true,
    //  showNotification: true,
    
    ); */
    startTimer();
  }

  play() async {
    //loop("splash.mp3");
    musicCache = AudioCache();
    audioPlayer = await musicCache.play("splash.mp3");
    //int result = await audioPlayer.play("assets/go.m4a");
  }

  startTimer() async {
    var duration = Duration(seconds: 10);
    return Timer(duration, () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            flex: 4,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Image(image: AssetImage('assets/talk.gif')),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(100),
                ),
                Text(
                  'Talk To Me',
                  style: TextStyle(
                      color: Colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(60)),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(20),
                ),
                Text(
                  'Where You Can Find\nThe Answers Of All Your Questions.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: ScreenUtil().setSp(35)),
                ),
              ],
            ),
          ),
          Expanded(
            child: ScalingText(
              'Loading...',
              style: TextStyle(
                  color: Colors.purple,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(60)),
            ),
          )
        ],
      ),
    );
  }
}

/* AnimatedSplashScreen(
      splash: Image(image: AssetImage('assets/a_welcome.png')),
      nextScreen: HomePage(),
      splashTransition: SplashTransition.fadeTransition,
      pageTransitionType: PageTransitionType.fade,
      backgroundColor: Colors.white,
      duration: 3,
    ); */
