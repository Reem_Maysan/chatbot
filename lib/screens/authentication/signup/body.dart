import 'package:chatbot/components/already_have_account.dart';
import 'package:chatbot/components/rounded_button.dart';
import 'package:chatbot/components/rounded_input_field.dart';
import 'package:chatbot/components/rounded_password.dart';
import 'package:chatbot/screens/authentication/login/logIn.dart';
import 'package:chatbot/screens/authentication/signup/background.dart';
import 'package:chatbot/screens/authentication/signup/or_divider.dart';
import 'package:chatbot/screens/authentication/signup/social_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          /*   Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ), */
         //   SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/log3.svg",
              height: size.height * 0.35,
            ),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () {},
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/twitter.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/google-plus.svg",
                  press: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}