import 'package:chatbot/screens/authentication/login/body.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
    );
  }
}































/* import 'package:chatbot/Animation/fadeAnimation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        // color:
        body: Container(
          child: Column(
            children: [
              Container(
                height: ScreenUtil().setHeight(600),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/background.png"), fit: BoxFit.fill)),
                child: Stack(
                  children: [
                    Positioned(
                      left: ScreenUtil().setWidth(40),
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setHeight(220),
                      child: FadeAnimation(
                        10,
                        Container(
                          width: ScreenUtil().setWidth(700),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  scale: 25.5,
                                  fit: BoxFit.fill,
                                  image: AssetImage("assets/rob4.gif"))),
                        ),
                      ),
                    ),
                    Positioned(
                      child: Container(
                        margin:
                            EdgeInsets.only(top: ScreenUtil().setHeight(80)),
                        child: Center(
                          child: Text(
                            'Login',
                            style: TextStyle(
                                color: Colors.purple,
                                fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil().setSp(60)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(30),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white24,
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 10),
                                blurRadius: 20,
                                color: Color.fromRGBO(143, 148, 251, 1))
                          ]),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Colors.grey[100]))),
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email or Phone number",
                                  hintStyle:
                                      TextStyle(color: Colors.grey[400])),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle:
                                      TextStyle(color: Colors.grey[400])),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(30),
                          ),
                          Container(
                              height: ScreenUtil().setHeight(80),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(143, 148, 251, 1),
                                    Color.fromRGBO(143, 148, 251, 6)
                                  ])),
                              child: Center(
                                child: GestureDetector(
                                  onTap: () {
                                    print('hi from Login!');
                                  },
                                  child: Text(
                                    'Login',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: ScreenUtil().setHeight(30),
                          ),
                          Text(
                            'Forget Password?',
                            style: TextStyle(
                              color: Color.fromRGBO(143, 148, 251, 1),
                            ),
                          ),
                        ],
                      ),
                    )
                 
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
 */