import 'package:chatbot/screens/chatting/sideBar/sideBar_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  void response(query) async {
    AuthGoogle authGoogle =
        await AuthGoogle(fileJson: "assets/service.json").build();
    Dialogflow dialogflow =
        Dialogflow(authGoogle: authGoogle, language: Language.english);
    AIResponse aiResponse = await dialogflow.detectIntent(query);
    setState(() {
      messsages.insert(0, {
        "data": 0,
        "message": aiResponse.getListMessage()[0]["text"]["text"][0].toString()
      });
    });

    print(aiResponse.getListMessage()[0]["text"]["text"][0].toString());
  }

  final messageInsert = TextEditingController();
  List<Map> messsages = List();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Stack(children: [
          Container(
            height: size.height,
            width: double.infinity,
            // Here i can use size.width but use double.infinity because both work as a same
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  top: 0,
                  left: 0,
                  child: Image.asset(
                    "assets/signup_top.png",
                    width: size.width * 0.35,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: Image.asset(
                    "assets/main_bottom.png",
                    width: size.width * 0.25,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Image.asset(
                    "assets/login_bottom.png",
                    width: size.width * 0.4,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 15, bottom: 10),
                      child: Text(
                        "Today, ${DateFormat("Hm").format(DateTime.now())}",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Flexible(
                        child: ListView.builder(
                            reverse: true,
                            itemCount: messsages.length,
                            itemBuilder: (context, index) => chat(
                                messsages[index]["message"].toString(),
                                messsages[index]["data"]))),
                    SizedBox(
                      height: ScreenUtil().setHeight(30),
                    ),
                    Divider(
                      height: ScreenUtil().setHeight(1),
                      color: Colors.pinkAccent,
                    ),
                    Container(
                      height: ScreenUtil().setHeight(100),
                      child: ListTile(
                        leading: IconButton(
                          icon: Icon(
                            Icons.camera_alt,
                            color: Colors.deepPurple,
                            size: 25,
                          ),
                          onPressed: () {},
                        ),
                        title: Container(
                          height: ScreenUtil().setHeight(80),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: Color.fromRGBO(220, 220, 220, 1),
                          ),
                          padding: EdgeInsets.only(left: 15, top: 13),
                          child: TextFormField(
                            controller: messageInsert,
                            decoration: InputDecoration(
                              hintMaxLines: null,
                              errorMaxLines: null,
                              helperMaxLines: null,
                              hintText: "Enter a Message...",
                              hintStyle: TextStyle(color: Colors.black26),
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                            ),
                            style: TextStyle(fontSize: 16, color: Colors.black),
                            onChanged: (value) {},
                          ),
                        ),
                        trailing: IconButton(
                            icon: Icon(
                              Icons.send,
                              size: 25.0,
                              color: Colors.deepPurple,
                            ),
                            onPressed: () {
                              if (messageInsert.text.isEmpty) {
                                print("empty message");
                              } else {
                                setState(() {
                                  messsages.insert(0, {
                                    "data": 1,
                                    "message": messageInsert.text
                                  });
                                });
                                //   response(messageInsert.text);
                                messageInsert.clear();
                              }
                              /*  FocusScopeNode currentFocus = FocusScope.of(context);
                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          } */
                            }),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(40))
                  ],
                ),
              ],
            ),
          ),
         // SideBar()
        ]),
      ),
    );
  }

  //for better one i have use the bubble package check out the pubspec.yaml

  Widget chat(String message, int data) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        mainAxisAlignment:
            data == 1 ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          data == 0
              ? Container(
                  height: 60,
                  width: 60,
                  child: CircleAvatar(
                    backgroundColor: Colors.yellow,
                    // backgroundImage: AssetImage("assets/robot.jpg"),
                  ),
                )
              : Container(),
          Padding(
            padding: EdgeInsets.only(top: 4.0, bottom: 0.0),
            child: Bubble(
                radius: Radius.circular(10.0),
                color: data == 0
                    ? Color.fromRGBO(23, 157, 139, 1)
                    : Colors.purple[200],
                elevation: 0.0,
                child: Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        width: 10.0,
                      ),
                      Flexible(
                          child: Container(
                        constraints: BoxConstraints(maxWidth: 200),
                        child: Text(
                          message,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ))
                    ],
                  ),
                )),
          ),
          data == 1
              ? Container(
                  //  decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/user.png"),fit: BoxFit.contain)),
                  height: ScreenUtil().setHeight(170),
                  //   width: ScreenUtil().setWidth(160),
                  child: Image(
                    image: AssetImage("assets/user.png"),
                    fit: BoxFit.contain,
                  ))
              : Container(),
        ],
      ),
    );
  }

/*   Container(
            height: size.height,
            width: double.infinity,
            // Here i can use size.width but use double.infinity because both work as a same
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  top: 0,
                  left: 0,
                  child: Image.asset(
                    "assets/signup_top.png",
                    width: size.width * 0.35,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: Image.asset(
                    "assets/main_bottom.png",
                    width: size.width * 0.25,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: Image.asset(
                    "assets/login_bottom.png",
                    width: size.width * 0.4,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 15, bottom: 10),
                      child: Text(
                        "Today, ${DateFormat("Hm").format(DateTime.now())}",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Flexible(
                        child: ListView.builder(
                            reverse: true,
                            itemCount: messsages.length,
                            itemBuilder: (context, index) => chat(
                                messsages[index]["message"].toString(),
                                messsages[index]["data"]))),
                    SizedBox(
                      height: ScreenUtil().setHeight(30),
                    ),
                    Divider(
                      height: ScreenUtil().setHeight(1),
                      color: Colors.pinkAccent,
                    ),
                    Container(
                      height: ScreenUtil().setHeight(100),
                      child: ListTile(
                        leading: IconButton(
                          icon: Icon(
                            Icons.camera_alt,
                            color: Colors.deepPurple,
                            size: 25,
                          ),
                          onPressed: () {},
                        ),
                        title: Container(
                          height: ScreenUtil().setHeight(80),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: Color.fromRGBO(220, 220, 220, 1),
                          ),
                          padding: EdgeInsets.only(left: 15, top: 13),
                          child: TextFormField(
                            controller: messageInsert,
                            decoration: InputDecoration(
                              hintMaxLines: null,
                              errorMaxLines: null,
                              helperMaxLines: null,
                              hintText: "Enter a Message...",
                              hintStyle: TextStyle(color: Colors.black26),
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                            ),
                            style: TextStyle(fontSize: 16, color: Colors.black),
                            onChanged: (value) {},
                          ),
                        ),
                        trailing: IconButton(
                            icon: Icon(
                              Icons.send,
                              size: 25.0,
                              color: Colors.deepPurple,
                            ),
                            onPressed: () {
                              if (messageInsert.text.isEmpty) {
                                print("empty message");
                              } else {
                                setState(() {
                                  messsages.insert(0, {
                                    "data": 1,
                                    "message": messageInsert.text
                                  });
                                });
                                //   response(messageInsert.text);
                                messageInsert.clear();
                              }
                              /*  FocusScopeNode currentFocus = FocusScope.of(context);
                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          } */
                            }),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(40))
                  ],
                ),
              ],
            ),
          ),
          */

/* 
  Widget chat2(String message, int data) {
  return  Column(
      children: <Widget>[
        Container(
          height: ScreenUtil().setHeight(100),
          width: 0.0,
          color: Colors.purple[200],
          child: Material(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: ScreenUtil().setHeight(100),
                  height: ScreenUtil().setHeight(100),
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      image: ExactAssetImage('assets/user.png'),
                      fit: BoxFit.cover,
                    ),
                    border: new Border.all(
                      color: Colors.white,
                      width: 4.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: ScreenUtil().setHeight(100),
          width: 0.0,
          child: Material(
            color: Colors.purple[200],
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(100 / 3),
              bottomRight: Radius.circular(100 / 3),
              topRight: Radius.circular(100 / 3),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
 */

}
