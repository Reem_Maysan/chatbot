import 'dart:async';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:chatbot/models/slide.dart';
import 'package:chatbot/screens/authentication/login/logIn.dart';
import 'package:chatbot/screens/authentication/signup/signUp.dart';
import 'package:chatbot/screens/chatting/chat.dart';
import 'package:chatbot/widgets/SlideItem.dart';
import 'package:chatbot/widgets/clipPart.dart';
import 'package:chatbot/widgets/slideDots.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPage = 0;
  bool firstEnter = true;
  final PageController _pageController = PageController(initialPage: 0);

  AudioPlayer audioPlayer;
  AudioCache musicCache;

  @override
  void initState() {
    super.initState();
    audioPlayer = AudioPlayer();
    if (firstEnter) {
      play();
      firstEnter = false;
    }
    Timer.periodic(Duration(seconds: 8), (Timer timer) {
      if (_currentPage < 2)
        _currentPage++;
      else
        _currentPage = 0;
      _pageController.animateToPage(_currentPage,
          duration: Duration(milliseconds: 300), curve: Curves.easeIn);
    });
  }

  play() async {
    musicCache = AudioCache();
    audioPlayer = await musicCache.loop("go.m4a");
    //int result = await audioPlayer.play("assets/go.m4a");
  }

  _onPageChange(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Colors.white,
          width: double.infinity,
          height: size.height,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/main_top.png",
                  width: size.width * 0.35,
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                child: Image.asset(
                  "assets/login_bottom.png",
                  width: size.width * 0.4,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  color: Colors.transparent,
                  /* 
                  decoration: BoxDecoration(
                      border: Border.all(
                        width: 4,
                      //  color: Colors.purple[500],
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(20))), */
                  child: Column(
                    children: [
                      Expanded(
                          child: Stack(
                              alignment: AlignmentDirectional.bottomCenter,
                              children: [
                            PageView.builder(
                              onPageChanged: _onPageChange,
                              scrollDirection: Axis.horizontal,
                              controller: _pageController,
                              itemCount: slideList.length,
                              itemBuilder: (context, index) =>
                                  SlideItem(index: index),
                            ),
                            Stack(
                              alignment: AlignmentDirectional.topStart,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 35),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      for (int i = 0; i < slideList.length; i++)
                                        if (i == _currentPage)
                                          SlideDots(true)
                                        else
                                          SlideDots(false)
                                    ],
                                  ),
                                )
                              ],
                            )
                          ])),
                      Column(
                        //   crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          RaisedButton(
                              splashColor: Colors.purpleAccent,
                              color: Colors.purple,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text(
                                'Getting Started',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(40),
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ChatPage()),
                                );
                              }),
                          /*    FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        color: Colors.purple,
                        padding: EdgeInsets.all(15),
                        child: Text(
                          'Getting Started',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(40),
                              color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ClipPart()),
                          );
                        },
                      ),  */
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Have an account?',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(35),
                                    color: Colors.purple),
                              ),
                              //  SizedBox(width: ScreenUtil().setWidth(60),),
                              FlatButton(
                                child: Text(
                                  'Login',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: ScreenUtil().setSp(40),
                                      color: Colors.purple),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()),
                                  );
                                },
                              ),
                            ],
                          ),
                          FlatButton(
                            child: Text(
                              'Signup',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: ScreenUtil().setSp(40),
                                  color: Colors.purple),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SignUpScreen()),
                              );
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          //color: Colors.white,
          //  child:
        ),
      ),
    );
  }
}
